import unittest

from txbp.ClearQATestingUsers import ClearQATestingUsers
from txbp.NewEmployerSignUpTest import NewEmployerTest
from txbp.NewJobSeekerSignUpTest import NewJobSeekerTest
from txbp.globals import usernames

global usernames


def main():
    tests = [
        # (unittest.TestLoader().loadTestsFromTestCase(BluePagesKnownJobSeekerTest)),
        (unittest.TestLoader().loadTestsFromTestCase(NewJobSeekerTest)),
        (unittest.TestLoader().loadTestsFromTestCase(NewEmployerTest)),
        # (unittest.TestLoader().loadTestsFromTestCase(ClearQATestingUsers))
    ]
    test_suite = unittest.TestSuite(tests)
    unittest.TextTestRunner(verbosity=2).run(test_suite)
    print "New usernames created: {}".format(usernames)


if __name__ == "__main__":
    main()
