PAGE_LOAD_MAX_WAIT = 10

BASE_URL = "https://bluepages.wpengine.com/"
BASE_URL_BASE_AUTH = "https://{username}:{password}@bluepages.wpengine.com/"
BASE_URL1 = "https://www.texasbluepages.com"

LOG_IN_TEXT = "Log In"
LOG_OUT_TEXT = "Log Out"
SIGN_UP_PAGE_TITLE = "Sign Up"
JOB_LISTINGS_TEXT = "Job Listings"
FOR_JOB_SEEKERS_LINK_TEXT = "For job seekers"
FIND_A_JOB_TEXT = "Find a Job"
LOGIN = "Login"
LOGOUT = "Logout"
REGISTER = "Register"
USER_URL = "user_url"
ROLE = "role"
NICKNAME = "nickname"
EXPERIENCE = "experience"
NICK_NAME = "nick_name"
USER_EMAIL = "user_email"
LAST_NAME = "last_name"
FIRST_NAME = "first_name"
USER_LOGIN = "user_login"
USERNAME = "username"
CONFIRM_USER_PASSWORD = "confirm_user_password"
USER_PASSWORD = "user_password"

FINANCE_EXPERIENCE = "Finance"
EMPLOYER_ROLE = "Employer"
JOB_SEEKER_ROLE = "Job Seeker"

NAME_TMPL = "{first} {last}"
LINKEDIN_URL_TMPL = "https://linkedin.com/in/{first}{last}"
NICK_TMPL = "{first}.{last}"

SUBMIT_XP_TMPL = '//input[@value="{}" and @type="submit"]'
LOGIN_BUTTON_XP = SUBMIT_XP_TMPL.format(LOGIN)
REGISTER_BUTTON_XP = SUBMIT_XP_TMPL.format(REGISTER)

INPUT_XP_TMPL = '//input[@data-key="{}"]'
USER_PASSWORD_FIELD_XP = INPUT_XP_TMPL.format(USER_PASSWORD)
CONFIRM_USER_PASSWORD_FIELD_XP = INPUT_XP_TMPL.format(CONFIRM_USER_PASSWORD)
USERNAME_FIELD_XP = INPUT_XP_TMPL.format(USERNAME)
USER_LOGIN_FIELD_XP = INPUT_XP_TMPL.format(USER_LOGIN)
FIRST_NAME_FIELD_XP = INPUT_XP_TMPL.format(FIRST_NAME)
LAST_NAME_FIELD_XP = INPUT_XP_TMPL.format(LAST_NAME)
USER_EMAIL_FIELD_XP = INPUT_XP_TMPL.format(USER_EMAIL)
ADMIN_INPUT_XP_TMPL = '//input[@class="input" and @id="{id}"]'

INPUT_TYPE_PASSWORD = '//input[@type="password"]'

LINK_XP_TMPL = '//a[text()="{}"]'
REGISTER_LINK_XP = LINK_XP_TMPL.format(REGISTER)
LOGIN_LINK_XP = LINK_XP_TMPL.format(LOGIN)
ROLE_PICKER_ARROW_XP = '//span[@class="select2-selection__arrow"]'
LINK_TITLE_XP_TMPL = '//a[@title="{}"]'
REGISTRATION_SUBMITTED_XP = '//div[@class="um-postmessage"]'
LIST_XP_TMPL = '//li[text()="{}"]'
# SELECT_XP_TMPL = '//select[@id="{}"]'
EXPERIENCE_FIELD_XP = '//li[@class="select2-search select2-search--inline"]'
JOB_LINK_TEXT_TMPL = '//a/div/h4[contains(text(),"{}")]'

PAGE_TITLE_TMPL = "{} | Texas Blue Pages"
TEXAS_BLUE_PAGES_TITLE = "Job Listings - Texas Blue Pages"
TEXAS_BLUE_PAGES_TITLE_TMPL = u"{} \u2013 Texas Blue Pages"

BLUE_PAGES_HOME_PAGE_TITLE = u"BluePages \u2013 Test Site"
BLUE_PAGES_ADMIN_LOGIN_TITLE = u'Log In \u2039 BluePages \u2014 WordPress'

BLUE_PAGES_TITLE_DASH_TMPL = u"{} \u2013 BluePages"
TEXAS_BLUE_PAGES_REGISTER_TITLE = BLUE_PAGES_TITLE_DASH_TMPL.format(REGISTER)
BLUE_PAGES_TITLE_VB_TMPL = u"{} | BluePages"
TITLE_XP_TMPL = u'//title[text()="{}"]'
TITLE_XPATH_TEMPLATE = TITLE_XP_TMPL.format(BLUE_PAGES_TITLE_DASH_TMPL)
BLUE_PAGES_TITLE_XP_TMPL = TITLE_XP_TMPL.format(BLUE_PAGES_TITLE_DASH_TMPL)
TITLE_VB_XPATH_TEMPLATE = TITLE_XP_TMPL.format(BLUE_PAGES_TITLE_VB_TMPL)
BLUE_PAGES_HOME_PAGE_TITLE_XP=TITLE_XP_TMPL.format(BLUE_PAGES_HOME_PAGE_TITLE)
BLUE_PAGES_REGISTER_TITLE_XP=TITLE_XP_TMPL.format(TEXAS_BLUE_PAGES_REGISTER_TITLE)
JOB_LISTINGS_PAGE_TITLE_XP = TITLE_XPATH_TEMPLATE.format(JOB_LISTINGS_TEXT)
LOGIN_PAGE_TITLE_XP = TITLE_XPATH_TEMPLATE.format(LOGIN)
TITLE_CONTAINS_XP_TMPL = u'//title[contains(text(),"{}")]'
ADMIN_LOGGED_IN_TITLE= u'Dashboard \u2039 BluePages \u2014 WordPress'
BLUE_PAGES_ADMIN_LOGIN_TITLE_XP = TITLE_XP_TMPL.format(BLUE_PAGES_ADMIN_LOGIN_TITLE)

JOB_SEEKER1_EMAIL = "txbp.job.seeker@zurga.com"
JOB_SEEKER1_NAME = NAME_TMPL.format(first="Mujo", last="Radnik")
JOB_SEEKER2_NAME = NAME_TMPL.format(first="Vlajo", last="Pendrek")
JOB_SEEKER2_NICK = "Vlajo_Pendrek"
JOB_SEEKER2_USERNAME = "vlajo"
JOB_LAWN_MOWER_NAME = "Lawn Mower"
TEST_EMPLOYER_FIRST_NAME = "Lyn{dtime}"
TEST_EMPLOYER_LAST_NAME = "Testemployer"
TEST_EMPLOYER_USERNAME = "tst_{dtime}_qa"
TEST_JOBSEEKER_FIRST_NAME = "Joe{dtime}"
TEST_JOBSEEKER_LAST_NAME = "Tester"
TEST_JOBSEEKER_USERNAME = "tst_{dtime}_qa"
TEST_EMAIL_TMPL = "{username}@texasbluepages.com"
DEMO_AUTH = "demo"
DEMO_PASS = "bluepages"
TEST_PASSWORD = "Texasb1ue"
TEST_ADMIN_USERNAME = "testadmin"
TEST_ADMIN_NICKNAME = "testadmin"
TEST_ADMIN_PASSWORD = "@^z1%bS&d2fx3bs3"
JOB_SEEKER2_PASSWORD = TEST_PASSWORD
RAND_ID_DATETIME_FORMAT = "%y%m%d%H%M%S%f"

TITLE_EXPECTED_BUT_FOUND = u'Title "{expected}" expected, found "{found}"'
ASSERT_MSG_TMPL = u'Assert {ex} on {line}:{filename} in "{text}"'

ADMIN_USERS_UPDATED_NOTICE_XP = '//div[contains(@class,"updated um-admin-notice notice") and @data-key="actions"]/p[contains(text(),"Users have been updated.")]'
ADMIN_USERS_DELETE_PROMPT_XP = '//div[@class="wrap"]/h1[text()="Delete Users"]/../ul/li/input[text()[contains(.,"{username}")]]'
ADMIN_USERS_APPLY_BUTTON_XP = '//input[@name="um_bulkedit" and @id="um_bulkedit" and @class="button" and @value="Apply" and @type="submit"]'
ADMIN_USERS_DELETE_APPLY_BUTTON_XP = '//input[@id="doaction" and @class="button action" and @value="Apply" and @type="submit"]'
ADMIN_USERS_APPROVE_MENU_SELECTION = '//select[@name="um_bulk_action[]" and @id="um_bulk_action" and @class=""]/option[@value="um_approve_membership" and text()="Approve Membership"]'
ADMIN_USERS_DELETE_MENU_SELECTION = '//select[@name="action" and @id="bulk-action-selector-top"]/option[@value="delete" and text()="Delete"]'
ADMIN_USERS_CHECKBOX_TMPL = '//label[@class="screen-reader-text" and text()="Select {}"]/following-sibling::input[@name="users[]" and @type="checkbox"]'
ADMIN_USERS_SEARCH_SUBTITLE_XP_TMPL = '//span[@class="subtitle" and contains(text(),"Search results for") and contains(text(), "{}")]'
ADMIN_USERS_SEARCH_BUTTON_XP = '//input[@id="search-submit" and @class="button" and @value="Search Users" and @type="submit"]'
ADMIN_USERS_SEARCH_FIELD_XP = '//input[@id="user-search-input" and @type="search"]'
ADMIN_SUBMENU_ALL_USERS = '//a[contains(text(),"All Users")]'
ADMIN_MENU_USERS = '//div[@class="wp-menu-name" and contains(text(),"Users")]'
TITLE_CONTAINS_DASHBOARD_XP = TITLE_CONTAINS_XP_TMPL.format("Dashboard")
