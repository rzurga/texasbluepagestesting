import time

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.action_chains import ActionChains

from txbp.BluePagesConst import PAGE_LOAD_MAX_WAIT, DEMO_PASS, DEMO_AUTH, INPUT_TYPE_PASSWORD


def init_se_driver(url):
    driver = webdriver.Firefox()
    driver.implicitly_wait(PAGE_LOAD_MAX_WAIT)  # seconds
    driver.get(url)
    return driver


def init_se_driver_basic_auth(url, username=DEMO_AUTH, password=DEMO_PASS):
    driver = init_se_driver(url)
    wait_alert(driver)
    alert_pop_up = driver.switch_to.alert
    alert_pop_up.send_keys(username)
    alert_pop_up.send_keys(Keys.TAB)
    password_field = driver.find_element_by_xpath(INPUT_TYPE_PASSWORD)
    # alert_pop_up.send_keys(password)
    password_field.send_keys(password)
    alert_pop_up.accept()
    return driver


def wait_for(condition_function):
    start_time = time.time()
    while time.time() < start_time + PAGE_LOAD_MAX_WAIT:
        if condition_function():
            return True
        else:
            time.sleep(0.1)
    raise Exception('Timeout waiting for {}'.format(condition_function.__name__))


class wait_for_page_load(object):
    """
    Based on http://www.obeythetestinggoat.com/how-to-get-selenium-to-wait-for-page-load-after-a-click.html
    """

    def __init__(self, driver):
        self.driver = driver

    def __enter__(self):
        self.old_page = self.driver.find_element_by_tag_name('html')
        pass

    def page_has_loaded(self):
        while True:
            try:
                new_page = self.driver.find_element_by_tag_name('html')
                return new_page.id != self.old_page.id
            except NoSuchElementException, e:
                pass

    def __exit__(self, *_):
        wait(self.page_has_loaded)


def load_and_wait(driver, link=None, click_on=None, wait_on=None, execute_script_click=False):
    try:
        with wait_for_page_load(driver):
            if link:
                return driver.get(link)
            elif click_on:
                driver.execute_script("arguments[0].scrollIntoView();", click_on)
                if execute_script_click:
                    return driver.execute_script("arguments[0].click();", click_on)
                else:
                    return click_on.click()
    finally:
        if wait_on is not None:
            # print u"load_and_wait: wait: {}".format(wait_on)
            wait(driver, wait_on)


def wait(driver, wait_on=None, wait_by=By.XPATH):
    if wait_on is not None:
        # print u"wait: {}: {}".format(wait_on, wait_by)
        element = None
        try:
            element = WebDriverWait(driver, PAGE_LOAD_MAX_WAIT, poll_frequency=0.1).until(
                EC.presence_of_element_located((wait_by, wait_on))
            )
        except TimeoutException, e:
            print u'Timed out waiting for "{}"'.format(wait_on)
        finally:
            return element


def wait_alert(driver):
    element = None
    try:
        element = WebDriverWait(driver, PAGE_LOAD_MAX_WAIT, poll_frequency=0.1).until(
            EC.alert_is_present()
        )
    except TimeoutException, e:
        print "Timed out waiting for alert"
    finally:
        return element


def hover(driver, element):
    hover = ActionChains(driver).move_to_element(element)
    hover.perform()


class Se:
    def __init__(self, url):
        self.driver = webdriver.Firefox()
        self.driver.get(url)

    def load_and_wait(self, link=None, click_on=None, wait_on=None):
        try:
            with wait_for_page_load(self.driver):
                if link:
                    return self.driver.get(link)
                elif click_on:
                    self.driver.execute_script("arguments[0].scrollIntoView();", click_on)
                    return click_on.click()
        finally:
            wait(self.driver, wait_on)

    def wait(self, wait_on=None, wait_by=By.XPATH):
        if wait_on is not None:
            element = None
            try:
                element = WebDriverWait(self.driver, PAGE_LOAD_MAX_WAIT).until(
                    EC.presence_of_element_located((wait_by, wait_on))
                )
            finally:
                return element

    def wait_alert(self):
        element = None
        try:
            element = WebDriverWait(self.driver, PAGE_LOAD_MAX_WAIT).until(
                EC.alert_is_present()
            )
        finally:
            return element
