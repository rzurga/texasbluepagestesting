import datetime
import random
import sys
import traceback

from txbp.BluePagesConst import RAND_ID_DATETIME_FORMAT


def get_assertion_location():
    _, _, tb = sys.exc_info()
    traceback.print_tb(tb)  # Fixed format
    tb_info = traceback.extract_tb(tb)
    filename, line, func, text = tb_info[0]
    return filename, line, text


def cblank(str):
    """
    cblank - coerce blank
    :param str:
    :return: if input string is None return "" (empty string), otherwise return original argment
    """
    if str is None:
        return ""
    else:
        return str


def get_rand_id():
    """
    get_rand_id returns a quasi random number that represents a the date/time up to the second and pads
    it with a 2-digit random number to try to ensure no collisions happen even if called twice in the same second
    :return: quasi random number that denotes current time
    """
    now = datetime.datetime.now()
    return "{0}{1:02d}".format(now.strftime(RAND_ID_DATETIME_FORMAT)[:-3], random.randint(0, 99))