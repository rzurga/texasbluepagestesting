import unittest

from txbp.BluePagesUtil import bluepages1_login, bluepages1_logout, bluepages1_job_listings, bluepages1_job_listing_job, \
    bluepages_login, bluepages_logout, bluepages_job_listings, bluepages_job_listing
from txbp.BluePagesConst import BASE_URL1, TEXAS_BLUE_PAGES_TITLE, JOB_LAWN_MOWER_NAME, JOB_SEEKER1_NAME, \
    JOB_SEEKER1_EMAIL, \
    TEST_PASSWORD, PAGE_TITLE_TMPL, DEMO_AUTH, DEMO_PASS, JOB_SEEKER2_USERNAME, JOB_SEEKER2_PASSWORD, \
    BASE_URL_BASE_AUTH, BLUE_PAGES_TITLE_VB_TMPL, JOB_SEEKER2_NICK, BLUE_PAGES_TITLE_DASH_TMPL, \
    TITLE_XPATH_TEMPLATE, BLUE_PAGES_HOME_PAGE_TITLE, FIND_A_JOB_TEXT, ASSERT_MSG_TMPL
from txbp.SeleniumUtil import init_se_driver, wait, wait_for_page_load
from txbp.Util import get_assertion_location


class BluePages1JobSeekerTest(unittest.TestCase):
    def setUp(self):
        self.driver = init_se_driver(BASE_URL1)
        self.assertEqual(TEXAS_BLUE_PAGES_TITLE, self.driver.title)

    def test_job_seeker(self):
        try:
            bluepages1_login(self.driver, username=JOB_SEEKER1_EMAIL, password=TEST_PASSWORD, name=JOB_SEEKER1_NAME)
            self.assertEquals(PAGE_TITLE_TMPL.format(JOB_SEEKER1_NAME), self.driver.title)
            bluepages1_job_listings(self.driver)
            bluepages1_job_listing_job(self.driver, job_name=JOB_LAWN_MOWER_NAME)
            bluepages1_logout(self.driver)
        except AssertionError, e:
            raise AssertionError(e)

    def tearDown(self):
        # close the browser window
        self.driver.quit()


class BluePagesKnownJobSeekerTest(unittest.TestCase):
    def setUp(self):
        # self.driver = init_se_driver_basic_auth(BASE_URL, DEMO_AUTH, DEMO_PASS)
        self.driver = init_se_driver(BASE_URL_BASE_AUTH.format(username=DEMO_AUTH, password=DEMO_PASS))
        wait_for_page_load(self.driver)
        self.assertEqual(BLUE_PAGES_HOME_PAGE_TITLE, self.driver.title)

    def test_job_seeker(self):
        try:
            bluepages_login(self.driver, username=JOB_SEEKER2_USERNAME, password=JOB_SEEKER2_PASSWORD,
                            nick=JOB_SEEKER2_NICK)
            self.assertEquals(BLUE_PAGES_TITLE_VB_TMPL.format(JOB_SEEKER2_NICK), self.driver.title)
            bluepages_job_listings(self.driver)
            self.assertEquals(BLUE_PAGES_TITLE_DASH_TMPL.format(FIND_A_JOB_TEXT), self.driver.title)
            bluepages_job_listing(self.driver, job_title=JOB_LAWN_MOWER_NAME)
            wait(self.driver, wait_on=TITLE_XPATH_TEMPLATE.format(JOB_LAWN_MOWER_NAME))
            self.assertEquals(BLUE_PAGES_TITLE_DASH_TMPL.format(JOB_LAWN_MOWER_NAME), self.driver.title)
            bluepages_logout(self.driver)
        except AssertionError, e:
            filename, line, text = get_assertion_location()
            raise AssertionError(ASSERT_MSG_TMPL.format(ex=e, line=line, filename=filename, text=text))

    def tearDown(self):
        # close the browser window
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
