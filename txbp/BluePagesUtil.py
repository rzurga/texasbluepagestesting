from datetime import datetime

from selenium.common.exceptions import NoSuchElementException

from txbp.BluePagesConst import LOGIN_BUTTON_XP, USER_PASSWORD_FIELD_XP, USERNAME_FIELD_XP, SIGN_UP_PAGE_TITLE, \
    LOG_IN_TEXT, TEXAS_BLUE_PAGES_REGISTER_TITLE, JOB_LINK_TEXT_TMPL, JOB_LISTINGS_TEXT, \
    FOR_JOB_SEEKERS_LINK_TEXT, LOG_OUT_TEXT, TITLE_XPATH_TEMPLATE, \
    LOGIN_PAGE_TITLE_XP, JOB_LISTINGS_PAGE_TITLE_XP, USER_LOGIN_FIELD_XP, FIRST_NAME_FIELD_XP, \
    LAST_NAME_FIELD_XP, USER_EMAIL_FIELD_XP, CONFIRM_USER_PASSWORD_FIELD_XP, REGISTER_BUTTON_XP, \
    PAGE_TITLE_TMPL, TEXAS_BLUE_PAGES_TITLE_TMPL, BLUE_PAGES_TITLE_XP_TMPL, \
    BLUE_PAGES_TITLE_DASH_TMPL, LOGIN_LINK_XP, LINK_XP_TMPL, LINK_TITLE_XP_TMPL, \
    JOB_LAWN_MOWER_NAME, \
    TITLE_VB_XPATH_TEMPLATE, BLUE_PAGES_HOME_PAGE_TITLE_XP, FIND_A_JOB_TEXT, LOGIN, \
    TITLE_EXPECTED_BUT_FOUND, LOGOUT, REGISTER, FIRST_NAME, LAST_NAME, USER_EMAIL, NAME_TMPL, REGISTER_LINK_XP, \
    INPUT_XP_TMPL, ROLE_PICKER_ARROW_XP, REGISTRATION_SUBMITTED_XP, LIST_XP_TMPL, USER_URL, ROLE, NICKNAME, EXPERIENCE, \
    NICK_NAME, EXPERIENCE_FIELD_XP, TEST_ADMIN_USERNAME, TEST_ADMIN_PASSWORD, ADMIN_LOGGED_IN_TITLE, \
    ADMIN_USERS_UPDATED_NOTICE_XP, ADMIN_USERS_APPLY_BUTTON_XP, \
    ADMIN_USERS_APPROVE_MENU_SELECTION, ADMIN_USERS_CHECKBOX_TMPL, ADMIN_USERS_SEARCH_SUBTITLE_XP_TMPL, \
    ADMIN_USERS_SEARCH_BUTTON_XP, ADMIN_USERS_SEARCH_FIELD_XP, ADMIN_SUBMENU_ALL_USERS, ADMIN_MENU_USERS, \
    TITLE_CONTAINS_DASHBOARD_XP, BLUE_PAGES_ADMIN_LOGIN_TITLE_XP, BLUE_PAGES_ADMIN_LOGIN_TITLE, \
    ADMIN_USERS_DELETE_MENU_SELECTION, ADMIN_USERS_DELETE_PROMPT_XP, ADMIN_INPUT_XP_TMPL, SUBMIT_XP_TMPL, \
    ADMIN_USERS_DELETE_APPLY_BUTTON_XP, BLUE_PAGES_HOME_PAGE_TITLE
from txbp.SeleniumUtil import load_and_wait, wait, hover
from txbp.Util import cblank
from txbp.globals import usernames

QA_USER_EMAIL_MATCH = "_qa@texasbluepages.com"
QA_USERS_XP_TMPL = '//td[@class="email column-email" and @data-colname="Email"]/a[contains(@href,"{email}")]/../../td[@data-colname="Username"]/strong/a'.format(
    email=QA_USER_EMAIL_MATCH)
QA_USER_XP_TMPL = '//td[@data-colname="Username"]/strong/a[text()="{username}"]/../../../th[@class="check-column"]/input[@type="checkbox"]'

global usernames


def bluepages_login(driver, username="", password="", nick=None, title=None, click_login=True, login_title=None):
    if click_login:
        wait(driver, LOGIN_LINK_XP)
        login_link = driver.find_element_by_xpath(LOGIN_LINK_XP)
        load_and_wait(driver, click_on=login_link, wait_on=LOGIN_PAGE_TITLE_XP)
    if not login_title:
        login_title = BLUE_PAGES_TITLE_DASH_TMPL.format(LOGIN)
    assert login_title in driver.title, TITLE_EXPECTED_BUT_FOUND.format(expected=login_title, found=driver.title)
    driver.find_element_by_xpath(USERNAME_FIELD_XP).send_keys(username)
    driver.find_element_by_xpath(USER_PASSWORD_FIELD_XP).send_keys(password)
    login_button = driver.find_element_by_xpath(LOGIN_BUTTON_XP)
    if title:
        load_and_wait(driver, click_on=login_button, wait_on=title)
    elif nick:
        load_and_wait(driver, click_on=login_button, wait_on=TITLE_VB_XPATH_TEMPLATE.format(nick))
    else:
        login_button.click()

count = 0

def bluepages_login2(driver, username="", password="", nick=None, title=None, click_login=True, login_title=None):
    global count
    if click_login:
        wait(driver, LOGIN_LINK_XP)
        count += 1
        try:
            login_link = driver.find_element_by_xpath(LOGIN_LINK_XP)
            load_and_wait(driver, click_on=login_link, wait_on=LOGIN_PAGE_TITLE_XP)
        except NoSuchElementException, e:
            pass
    if not login_title:
        login_title = BLUE_PAGES_TITLE_DASH_TMPL.format(LOGIN)
    assert driver.title in login_title, TITLE_EXPECTED_BUT_FOUND.format(expected=login_title, found=driver.title)
    driver.find_element_by_xpath('//input[@data-key="username" or @id="user_login"]').send_keys(username)
    driver.find_element_by_xpath('//input[@data-key="user_password" or @id="user_pass"]').send_keys(password)
    login_button = driver.find_element_by_xpath('//input[(@value="Login" or @value="Log In") and @type="submit"]')
    if title:
        load_and_wait(driver, click_on=login_button, wait_on=title)
    elif nick:
        load_and_wait(driver, click_on=login_button, wait_on=TITLE_VB_XPATH_TEMPLATE.format(nick))
    else:
        login_button.click()


def bluepages_admin_login(driver, username="", password="", nick=None, title=None, click_login=True, login_title=None):
    if click_login:
        wait(driver, LOGIN_LINK_XP)
        load_and_wait(driver, click_on=(driver.find_element_by_xpath(LOGIN_LINK_XP)), wait_on=LOGIN_PAGE_TITLE_XP)
    if not login_title:
        login_title = BLUE_PAGES_TITLE_DASH_TMPL.format(LOGIN)
    assert login_title in driver.title, TITLE_EXPECTED_BUT_FOUND.format(expected=login_title, found=driver.title)
    driver.find_element_by_xpath(ADMIN_INPUT_XP_TMPL.format(id="user_login")).send_keys(username)
    driver.find_element_by_xpath(ADMIN_INPUT_XP_TMPL.format(id="user_pass")).send_keys(password)
    login_button = driver.find_element_by_xpath(SUBMIT_XP_TMPL.format("Log In"))
    if title:
        load_and_wait(driver, click_on=login_button, wait_on=title)
    elif nick:
        load_and_wait(driver, click_on=login_button, wait_on=TITLE_VB_XPATH_TEMPLATE.format(nick))
    else:
        login_button.click()


def bluepages_logout(driver):
    logout_button = driver.find_element_by_xpath(LINK_XP_TMPL.format(LOGOUT))
    load_and_wait(driver, click_on=logout_button, wait_on=BLUE_PAGES_HOME_PAGE_TITLE_XP)


def bluepages_logout_admin(driver):
    howdy_admin = driver.find_element_by_xpath('//a[@class="ab-item" and contains(text(), "Howdy, ")]')
    hover(driver, howdy_admin)
    logout_button = driver.find_element_by_xpath('//a[@class="ab-item" and text()="{}"]'.format(LOG_OUT_TEXT))
    load_and_wait(driver, click_on=logout_button, execute_script_click=True, wait_on=BLUE_PAGES_ADMIN_LOGIN_TITLE_XP)
    assert BLUE_PAGES_ADMIN_LOGIN_TITLE == driver.title, TITLE_EXPECTED_BUT_FOUND.format(
        expected=BLUE_PAGES_ADMIN_LOGIN_TITLE, found=driver.title)


def bluepages_job_listings(driver):
    find_a_job_link = wait(driver, LINK_XP_TMPL.format(FIND_A_JOB_TEXT))
    load_and_wait(driver, click_on=find_a_job_link, wait_on=TITLE_XPATH_TEMPLATE.format("Find a Job"))


def bluepages_job_listing(driver, job_title):
    wait(driver, wait_on=TITLE_XPATH_TEMPLATE.format(FIND_A_JOB_TEXT))
    job_listing = wait(driver, wait_on=LINK_TITLE_XP_TMPL.format(JOB_LAWN_MOWER_NAME))
    load_and_wait(driver, click_on=job_listing, wait_on=BLUE_PAGES_TITLE_XP_TMPL.format(job_title),
                  execute_script_click=True)
    wait(driver, wait_on=BLUE_PAGES_TITLE_XP_TMPL.format(job_title))


def bluepages_signup(driver, username="", password="", **kwargs):
    wait(driver, wait_on=REGISTER_LINK_XP)
    load_and_wait(driver, click_on=(driver.find_element_by_xpath(REGISTER_LINK_XP)),
                  wait_on=BLUE_PAGES_TITLE_XP_TMPL.format(REGISTER))
    assert TEXAS_BLUE_PAGES_REGISTER_TITLE in driver.title, TITLE_EXPECTED_BUT_FOUND.format(
        expected=BLUE_PAGES_TITLE_DASH_TMPL.format(REGISTER), found=driver.title)
    driver.find_element_by_xpath(USER_LOGIN_FIELD_XP).send_keys(username)
    driver.find_element_by_xpath(FIRST_NAME_FIELD_XP).send_keys(kwargs[FIRST_NAME])
    driver.find_element_by_xpath(LAST_NAME_FIELD_XP).send_keys(kwargs[LAST_NAME])
    driver.find_element_by_xpath(INPUT_XP_TMPL.format(NICKNAME)).send_keys(cblank(kwargs[NICK_NAME]))
    driver.find_element_by_xpath(USER_EMAIL_FIELD_XP).send_keys(cblank(kwargs[USER_EMAIL]))
    wait(driver, ROLE_PICKER_ARROW_XP).click()
    wait(driver, LIST_XP_TMPL.format(cblank(kwargs[ROLE]))).click()
    driver.find_element_by_xpath(INPUT_XP_TMPL.format(USER_URL)).send_keys(cblank(kwargs[USER_URL]))
    wait(driver, EXPERIENCE_FIELD_XP).click()
    wait(driver, LIST_XP_TMPL.format(cblank(kwargs[EXPERIENCE]))).click()
    driver.find_element_by_xpath(USER_PASSWORD_FIELD_XP).send_keys(password)
    driver.find_element_by_xpath(CONFIRM_USER_PASSWORD_FIELD_XP).send_keys(password)
    print "{tstamp} Adding user {username}: {email}".format(tstamp=datetime.now().strftime("%Y-%m-%d %H:%M"),
                                                            username=username, email=kwargs[USER_EMAIL])
    load_and_wait(driver, click_on=(driver.find_element_by_xpath(REGISTER_BUTTON_XP)),
                  wait_on=REGISTRATION_SUBMITTED_XP)
    usernames.append(username)


def bluepages_confirm_user(driver, username):
    bluepages_login(driver, username=TEST_ADMIN_USERNAME, password=TEST_ADMIN_PASSWORD,
                    title=TITLE_CONTAINS_DASHBOARD_XP)
    assert ADMIN_LOGGED_IN_TITLE == driver.title, TITLE_EXPECTED_BUT_FOUND.format(expected=ADMIN_LOGGED_IN_TITLE,
                                                                                  found=driver.title)
    driver.find_element_by_xpath(ADMIN_MENU_USERS).click()
    driver.find_element_by_xpath(ADMIN_SUBMENU_ALL_USERS).click()
    search_field = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_FIELD_XP)
    # driver.execute_script("arguments[0].scrollIntoView();", search_field)
    search_field.send_keys(username)
    search_users_button = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_BUTTON_XP)
    load_and_wait(driver, click_on=search_users_button, execute_script_click=True,
                  wait_on=ADMIN_USERS_SEARCH_SUBTITLE_XP_TMPL.format(username))
    user_checkbox = driver.find_element_by_xpath(ADMIN_USERS_CHECKBOX_TMPL.format(username))
    user_id = user_checkbox.get_attribute('value')
    print "Approving user: #{}: {}".format(user_id, username)
    user_checkbox.click()
    driver.find_element_by_xpath(ADMIN_USERS_APPROVE_MENU_SELECTION).click()
    apply_button = driver.find_element_by_xpath(
        ADMIN_USERS_APPLY_BUTTON_XP)
    load_and_wait(driver, click_on=apply_button, execute_script_click=True, wait_on=ADMIN_USERS_UPDATED_NOTICE_XP)
    bluepages_logout_admin(driver)


def bluepages_find_qa_username(driver, username):
    bluepages_login(driver, username=TEST_ADMIN_USERNAME, password=TEST_ADMIN_PASSWORD,
                    title=TITLE_CONTAINS_DASHBOARD_XP)
    assert ADMIN_LOGGED_IN_TITLE == driver.title, TITLE_EXPECTED_BUT_FOUND.format(expected=ADMIN_LOGGED_IN_TITLE,
                                                                                  found=driver.title)
    driver.find_element_by_xpath(ADMIN_MENU_USERS).click()
    driver.find_element_by_xpath(ADMIN_SUBMENU_ALL_USERS).click()
    search_field = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_FIELD_XP)
    # driver.execute_script("arguments[0].scrollIntoView();", search_field)
    search_field.send_keys("_qa@texasbluepages.com")
    search_users_button = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_BUTTON_XP)
    load_and_wait(driver, click_on=search_users_button, execute_script_click=True,
                  wait_on=ADMIN_USERS_SEARCH_SUBTITLE_XP_TMPL.format("_qa@texasbluepages.com"))
    user_checkbox = driver.find_element_by_xpath(
        '//td[@class="email column-email" and @data-colname="Email"]/a[match(@href,"mailto:.*_qa@texasbluepages.com")]/../')
    user_id = user_checkbox.get_attribute('value')
    print "Approving user: #{}: {}".format(user_id, username)


def bluepages_find_qa_usernames(driver):
    bluepages_login2(driver, username=TEST_ADMIN_USERNAME, password=TEST_ADMIN_PASSWORD,
                    title=TITLE_CONTAINS_DASHBOARD_XP)
    assert ADMIN_LOGGED_IN_TITLE == driver.title, TITLE_EXPECTED_BUT_FOUND.format(expected=ADMIN_LOGGED_IN_TITLE,
                                                                                  found=driver.title)
    driver.find_element_by_xpath(ADMIN_MENU_USERS).click()
    driver.find_element_by_xpath(ADMIN_SUBMENU_ALL_USERS).click()
    search_field = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_FIELD_XP)
    search_field.send_keys(QA_USER_EMAIL_MATCH)
    search_users_button = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_BUTTON_XP)
    load_and_wait(driver, click_on=search_users_button, execute_script_click=True,
                  wait_on=ADMIN_USERS_SEARCH_SUBTITLE_XP_TMPL.format(QA_USER_EMAIL_MATCH))
    user_names = driver.find_elements_by_xpath(QA_USERS_XP_TMPL)
    users = [x.text for x in user_names]
    bluepages_logout_admin(driver)
    return users


def bluepages_delete_user(driver, username):
    # bluepages_admin_login(driver, username=TEST_ADMIN_USERNAME, password=TEST_ADMIN_PASSWORD,
    #                       # title=TITLE_CONTAINS_DASHBOARD_XP, click_login=False,
    #                       title=TITLE_CONTAINS_DASHBOARD_XP,
    #                       # login_title=BLUE_PAGES_HOME_PAGE_TITLE)
    #                       )
    bluepages_login2(driver, username=TEST_ADMIN_USERNAME, password=TEST_ADMIN_PASSWORD,
                     title=[TITLE_CONTAINS_DASHBOARD_XP, BLUE_PAGES_ADMIN_LOGIN_TITLE],
                     login_title=BLUE_PAGES_ADMIN_LOGIN_TITLE,
                     click_login=False)
    assert ADMIN_LOGGED_IN_TITLE == driver.title, TITLE_EXPECTED_BUT_FOUND.format(expected=ADMIN_LOGGED_IN_TITLE,
                                                                                  found=driver.title)
    driver.find_element_by_xpath(ADMIN_MENU_USERS).click()
    driver.find_element_by_xpath(ADMIN_SUBMENU_ALL_USERS).click()
    search_field = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_FIELD_XP)
    search_field.send_keys(username)
    search_users_button = driver.find_element_by_xpath(ADMIN_USERS_SEARCH_BUTTON_XP)
    load_and_wait(driver, click_on=search_users_button, execute_script_click=True,
                  wait_on=ADMIN_USERS_SEARCH_SUBTITLE_XP_TMPL.format(username))
    print "Removing username: {}".format(username)
    user_checkbox = driver.find_element_by_xpath(QA_USER_XP_TMPL.format(username=username))
    user_checkbox.click()
    driver.find_element_by_xpath(ADMIN_USERS_DELETE_MENU_SELECTION).click()
    apply_button = driver.find_element_by_xpath(ADMIN_USERS_DELETE_APPLY_BUTTON_XP)
    load_and_wait(driver, click_on=apply_button, execute_script_click=True,
                  wait_on=ADMIN_USERS_DELETE_PROMPT_XP.format(username=username))
    confirm_deletion = driver.find_element_by_xpath(
        '//input[@type="submit" and @id="submit" and @value="Confirm Deletion"]')
    load_and_wait(driver, click_on=confirm_deletion, execute_script_click=True,
                  wait_on='//div[@id="message"]/p[text()="User deleted."]')
    bluepages_logout_admin(driver)


def bluepages1_login(driver, username="", password="", name=""):
    login_link = driver.find_element_by_partial_link_text(LOG_IN_TEXT)
    load_and_wait(driver, click_on=login_link, wait_on=LOGIN_PAGE_TITLE_XP)
    login_title = TEXAS_BLUE_PAGES_TITLE_TMPL.format(LOGIN)
    assert login_title in driver.title, TITLE_EXPECTED_BUT_FOUND.format(expected=login_title, found=driver.title)
    username_field = driver.find_element_by_xpath(USERNAME_FIELD_XP)
    username_field.send_keys(username)
    password_field = driver.find_element_by_xpath(USER_PASSWORD_FIELD_XP)
    password_field.send_keys(password)
    login_button = driver.find_element_by_xpath(LOGIN_BUTTON_XP)
    load_and_wait(driver, click_on=login_button, wait_on=TITLE_XPATH_TEMPLATE.format(name))


def bluepages1_logout(driver):
    logout_button = driver.find_element_by_partial_link_text(LOG_OUT_TEXT)
    load_and_wait(driver, click_on=logout_button, wait_on=JOB_LISTINGS_PAGE_TITLE_XP)


def bluepages1_job_listings(driver):
    wait(driver, FOR_JOB_SEEKERS_LINK_TEXT)
    for_job_seekers = driver.find_element_by_partial_link_text(FOR_JOB_SEEKERS_LINK_TEXT)
    for_job_seekers.click()
    job_listings = driver.find_element_by_partial_link_text(JOB_LISTINGS_TEXT)
    load_and_wait(driver, click_on=job_listings, wait_on=JOB_LISTINGS_PAGE_TITLE_XP)


def bluepages1_job_listing_job(driver, job_name=""):
    lawn_mower = driver.find_element_by_xpath(JOB_LINK_TEXT_TMPL.format(job_name))
    load_and_wait(driver, click_on=lawn_mower, wait_on=TITLE_XPATH_TEMPLATE.format(job_name))


def bluepages1_signup(driver, username="", password="", **kwargs):
    signup_link = driver.find_element_by_partial_link_text(SIGN_UP_PAGE_TITLE)
    load_and_wait(driver, click_on=signup_link, wait_on=TITLE_XPATH_TEMPLATE.format(REGISTER))
    assert TEXAS_BLUE_PAGES_REGISTER_TITLE in driver.title, TITLE_EXPECTED_BUT_FOUND.format(
        expected=TEXAS_BLUE_PAGES_REGISTER_TITLE, found=driver.title)
    user_login_field = driver.find_element_by_xpath(USER_LOGIN_FIELD_XP)
    user_login_field.send_keys(username)
    first_name_field = driver.find_element_by_xpath(FIRST_NAME_FIELD_XP)
    first_name = kwargs[FIRST_NAME]
    first_name_field.send_keys(first_name)
    last_name_field = driver.find_element_by_xpath(LAST_NAME_FIELD_XP)
    last_name = kwargs[("%s" % LAST_NAME)]
    last_name_field.send_keys(last_name)
    user_email_field = driver.find_element_by_xpath(USER_EMAIL_FIELD_XP)
    user_email_field.send_keys(kwargs[USER_EMAIL])
    password_field = driver.find_element_by_xpath(USER_PASSWORD_FIELD_XP)
    password_field.send_keys(password)
    confirm_password_field = driver.find_element_by_xpath(CONFIRM_USER_PASSWORD_FIELD_XP)
    confirm_password_field.send_keys(password)
    register_button = driver.find_element_by_xpath(REGISTER_BUTTON_XP)
    print "Adding user {}".format(username)
    load_and_wait(driver, click_on=register_button,
                  wait_on=PAGE_TITLE_TMPL.format(NAME_TMPL.format(first=first_name, last=last_name)))
