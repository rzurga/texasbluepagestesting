import unittest

from txbp.BluePagesConst import BASE_URL_BASE_AUTH, DEMO_AUTH, DEMO_PASS, BLUE_PAGES_HOME_PAGE_TITLE
from txbp.BluePagesUtil import bluepages_delete_user, bluepages_find_qa_usernames
from txbp.SeleniumUtil import init_se_driver, wait_for_page_load
from txbp.globals import usernames

global usernames


class ClearQATestingUsers(unittest.TestCase):
    def setUp(self):
        # Open browser on the target home page
        self.driver = init_se_driver(BASE_URL_BASE_AUTH.format(username=DEMO_AUTH, password=DEMO_PASS))
        wait_for_page_load(self.driver)
        self.assertEqual(BLUE_PAGES_HOME_PAGE_TITLE, self.driver.title)

    def test_cleanup_qa(self):
        while True:
            usernames = bluepages_find_qa_usernames(self.driver)
            if len(usernames) == 0:
                break
            print "Users to remove: {}".format(usernames)
            for username in usernames:
                print "delete user: {}".format(username)
                bluepages_delete_user(self.driver, username)

    def tearDown(self):
        # close the browser window
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
