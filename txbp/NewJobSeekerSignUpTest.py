import unittest

from txbp.BluePagesConst import TEST_PASSWORD, TEST_EMAIL_TMPL, DEMO_AUTH, DEMO_PASS, \
    BASE_URL_BASE_AUTH, BLUE_PAGES_REGISTER_TITLE_XP, BLUE_PAGES_HOME_PAGE_TITLE, NAME_TMPL, \
    LINKEDIN_URL_TMPL, NICK_TMPL, JOB_SEEKER_ROLE, TEST_JOBSEEKER_LAST_NAME, TEST_JOBSEEKER_FIRST_NAME, \
    TEST_JOBSEEKER_USERNAME, FINANCE_EXPERIENCE
from txbp.BluePagesUtil import bluepages_logout, bluepages_signup, \
    bluepages_confirm_user
from txbp.Util import get_rand_id
from txbp.SeleniumUtil import init_se_driver, wait_for_page_load, wait


class NewJobSeekerTest(unittest.TestCase):
    def setUp(self):
        self.driver = init_se_driver(BASE_URL_BASE_AUTH.format(username=DEMO_AUTH, password=DEMO_PASS))
        wait_for_page_load(self.driver)
        self.assertEqual(BLUE_PAGES_HOME_PAGE_TITLE, self.driver.title)

    def test_new_job_seeker_signup(self):
        try:
            username = self.new_job_seeker_signup()
            # job seeker is now registered and needs confirmation
            bluepages_confirm_user(self.driver, username)
        except AssertionError, e:
            raise AssertionError(e.args)

    def new_job_seeker_signup(self):
        dtime = get_rand_id()
        username = TEST_JOBSEEKER_USERNAME.format(dtime=dtime)
        first_name = TEST_JOBSEEKER_FIRST_NAME.format(dtime=dtime)
        last_name = TEST_JOBSEEKER_LAST_NAME
        user_email = TEST_EMAIL_TMPL.format(username=username)
        bluepages_signup(self.driver, username=username, password=TEST_PASSWORD,
                         role=JOB_SEEKER_ROLE,
                         name=NAME_TMPL.format(first=first_name, last=last_name), first_name=first_name,
                         last_name=last_name, nick_name=NICK_TMPL.format(first=first_name, last=last_name),
                         user_email=user_email,
                         user_url=LINKEDIN_URL_TMPL.format(first=first_name, last=last_name),
                         experience=FINANCE_EXPERIENCE)
        wait(self.driver, wait_on=BLUE_PAGES_REGISTER_TITLE_XP)
        bluepages_logout(self.driver)
        return username

    def tearDown(self):
        # close the browser window
        self.driver.quit()


if __name__ == '__main__':
    unittest.main()
